/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.jasper.tagplugins.jstl.ForEach;

/**
 *
 * @author IK
 */
@WebServlet(urlPatterns = {"/test"})
public class test extends HttpServlet {

        // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    
    
    private String getContentFromUrl(String url) throws MalformedURLException, IOException{
          URL searchUrl = new URL(url);
          
              URLConnection conn = searchUrl.openConnection();
            // Look like faking the request coming from Web browser solve 403 error
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
           
             StringBuilder stringBuilder = new StringBuilder();
           
            String inputLine;
               while ((inputLine = in.readLine()) != null){
            byte[] bytes = inputLine.getBytes("UTF-8");
            stringBuilder.append( new String(bytes, "UTF-8"));
        }
             String outputUtf8 = stringBuilder.toString();
            
            in.close();
            return outputUtf8;   
    }
    
    
     private ArrayList<Song> parseSearchResults(String html) throws IOException {
          String[] parts = html.split("<h2>Interpreti</h2>");
       ArrayList<Song> out = new ArrayList<Song>();   
       if(parts.length > 1){
        String[] interprets = parts[1].split("<li><a href=\"");
        
        
        
        if(interprets.length > 1){
            String s = interprets[1];
             String[] a = s.split("\"");
            String[] c = s.split(">");
           // o.append(c.length);
            
            if(c.length > 1){
            String[] d = c[1].split("<");
     
         //   song.setTitle(d[0]);
   
          String pageInterpret = this.getContentFromUrl("http://www.karaoketexty.cz"+a[0]);
          
          String[] xx = pageInterpret.split("<table class=\"album\">");
     
          if(xx.length > 0){
          String[] aa = xx[1].split("<a href=\"/texty-pisni");
          int j = 0;
       //   o.append(aa.length);
          for(int i = 0; i < Math.min(aa.length, 11); i++){
              if(j > 0){
                  String[] bb = aa[i].split("\"  >");
                //  o.append(bb.length + "<br>");
                  if(bb.length == 2){
                   String[] cc = bb[1].split("<");
            String pageSong = this.getContentFromUrl("http://www.karaoketexty.cz/texty-pisni"+bb[0]);

               Song song = new Song();
               song.setTitle(cc[0]);
                String[] aaa = pageSong.split("<p class=\"text\">");
            String[] bbb = aaa[1].split("</p>");

             song.setText(bbb[0]);
             out.add(song); 
                  }else{
                      break;
                  }
              }
              j++;
                  
            }
            }
                }
        }
        
       }
        
 
        return out;
    }
     
     
     
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String interpret = request.getParameter("interpret");
       
       StringBuilder o = new StringBuilder(); 
       
       if(interpret != null){
        
        
        String html = this.getContentFromUrl("http://www.karaoketexty.cz/search?q="+URLEncoder.encode(interpret, "UTF-8"));

        ArrayList<Song> texts = this.parseSearchResults(html);
   
        
            
        
           if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }        
   
        try {    

        
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");

            // create an instance of a Sentence Splitter processing resource
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            
                      // create an instance of a Sentence Splitter processing resource
            ProcessingResource gazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
       
            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
             annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
             annotationPipeline.add(gazetteer);

            
      
     Corpus corpus = Factory.newCorpus("");
            for (Song song : texts) {
                Document document = Factory.newDocument(song.getText());
                corpus.add(document);
            }
     
          
            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for(int i=0; i< corpus.size(); i++){
                o.append("<h2>"+ texts.get(i).getTitle() +"</h2><br>");
                o.append("<table border=1 cellpadding=5 cellspacing=0><tr><th>Count words</th><th>Tokens total</th><th>Love</th><th>Dark</th><th>Found tokens</th></tr>");
                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();

                FeatureMap futureMap = null;
                // get all Token annotations
                AnnotationSet annSetTokens = as_default.get("Token", futureMap);
                AnnotationSet annSetTokensSong = as_default.get("Songs", futureMap);
             
                ArrayList tokenAnnotations = new ArrayList(annSetTokens);
                ArrayList songAnnotations = new ArrayList(annSetTokensSong);
                int countLove = 0;
                int countDark = 0;
                StringBuilder tokens = new StringBuilder();
                // looop through the Token annotations
                for(int j = 0; j < songAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation)songAnnotations.get(j);

                    // get the underlying string for the Token
                   
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    String t = "";
                    if(token.getFeatures().get("minorType").equals("love")){
                        t = "<span style=\"color: red; \">"+underlyingString+"</span>";
                        countLove++;
                    }else{
                        t = underlyingString;
                        countDark++;
                    }
                    tokens.append(t+", ");
                 //   o.append("Song: " + underlyingString+" - "+ token.getFeatures().get("minorType") +"<br>");
 
         
                }
                   o.append("<tr><td>" + annSetTokens.size()+"</td><td>" + annSetTokensSong.size() + "</td>"
                           + "<td>"+countLove+"</td><td>"+countDark+"</td><td>"+tokens.toString()+"</td></tr>");
            
o.append("</table><hr>");

            }
            
        } catch (GateException ex) {
            
        }
        
       }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String intr = request.getParameter("interpret");
            String form = "<form action=\"\" method=\"get\">"
                    + "Interpret: <input type=\"text\" name=\"interpret\" value=\""+(intr != null ? intr : "")+"\">"
                    + "<input type=\"submit\" value=\"Odeslat\">"
                    + "</form><hr>";
            out.println(form);
            out.println(o);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

   

       private void initialiseGate() {
        System.out.println("init gate");
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("C:\\gate");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("C:\\gate\\plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("C:\\gate", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
        
        } catch (GateException ex) {
           
        }
    }  
    
}


class Song{
    
    private String title;
    private String text; 
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
       
}